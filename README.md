# 💣 Clicker 💣

The classic game strikes back! Just kidding. It's my long favorite pastime since then: left click, right click and double click (or maybe no right clicks and double clicks at all, if I'm lazy ✌️). Being a longtime Minesweeper player, this is my attempt re-creating the classics, built in React and TypeScript (anyways, it's my first time learning React too).

You can configure the board anytime you want: `height`, `width` and `noOfBombs`. The `height` and `width`'s minimum value is 9. As of `noOfBombs`, it is restricted to between 5% to 40% of total number of squares (`width` $`\times`$ `height`).

The interface itself isn't perfect yet, but the gameplay is at least viable. I'd like to consider the labels' colors, fonts and board theme though. Unit tests might be good too, sorta?

You can start playing by installing the required dependencies (`npm install`) first, and then running `npm start` or build the project by running `npm run build`. This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
