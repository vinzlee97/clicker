import React from "react";

/**
 * {@link ClickableSquare} props which is intended to be used in board's square array.
 */
export class SquareProps {
  key: string;
  readonly rowIdx: RowIndex;
  readonly colIdx: ColIndex;
  /**
   * Since this is a square, the value consists of -1 to 8.
   *
   * 💣 -1 means this is a dangerous square to step on 💣
   */
  value: number = 0;
  clicked: boolean = false;
  marked: boolean = false;

  /**
   * Eight or lesser neighbors surrounding the square
   */
  readonly neighbors: Partial<SquareNeighbors> = {};

  constructor(
    _height = 0,
    _width = 0,
    _rowIdx = 0,
    _colIdx = 0,
    _value = 0,
    _clicked = false,
    _marked = false
  ) {
    this.key = `grid${_rowIdx * _width + _colIdx}`;
    this.rowIdx = _rowIdx;
    this.colIdx = _colIdx;
    this.value = _value;
    this.clicked = _clicked;
    this.marked = _marked;

    this.neighbors = {
      T: [_colIdx, _rowIdx - 1],
      L: [_colIdx - 1, _rowIdx],
      R: [_colIdx + 1, _rowIdx],
      B: [_colIdx, _rowIdx + 1],
      TL: [_colIdx - 1, _rowIdx - 1],
      TR: [_colIdx + 1, _rowIdx - 1],
      BL: [_colIdx - 1, _rowIdx + 1],
      BR: [_colIdx + 1, _rowIdx + 1],
    };

    // if this is the upmost grid
    if (_rowIdx - 1 < 0) {
      delete this.neighbors.T;
      delete this.neighbors.TL;
      delete this.neighbors.TR;
    }

    // if this is the lowest grid
    if (_rowIdx + 1 >= _height) {
      delete this.neighbors.B;
      delete this.neighbors.BL;
      delete this.neighbors.BR;
    }

    // if this is the far left
    if (_colIdx - 1 < 0) {
      delete this.neighbors.L;
      delete this.neighbors.TL;
      delete this.neighbors.BL;
    }

    // if this is the far right
    if (_colIdx + 1 >= _width) {
      delete this.neighbors.R;
      delete this.neighbors.TR;
      delete this.neighbors.BR;
    }
  }
}

/**
 * {@link SquareProps ClickableSquare props} with interaction callbacks, such as: single left click, double left click and single right click.
 *
 * Intended to be used for {@link ClickableSquare} internally, to be separate from board array's use of {@link SquareProps}.
 */
class SquarePropsCallback extends SquareProps {
  onClick: ([col, row]: [ColIndex, RowIndex]) => void;
  onRightClick: ([col, row]: [ColIndex, RowIndex]) => void;
  onDoubleClick: ([col, row]: [ColIndex, RowIndex]) => void;
  constructor(
    _height = 0,
    _width = 0,
    _rowIdx = 0,
    _colIdx = 0,
    _onClick: ([col, row]: [ColIndex, RowIndex]) => void,
    _onRightClick: ([col, row]: [ColIndex, RowIndex]) => void,
    _onDoubleClick: ([col, row]: [ColIndex, RowIndex]) => void,
    _value = 0,
    _clicked = false,
    _marked = false
  ) {
    super(_height, _width, _rowIdx, _colIdx, _value, _clicked, _marked);
    this.onClick = _onClick;
    this.onRightClick = _onRightClick;
    this.onDoubleClick = _onDoubleClick;
  }
}

export type SquareNeighbors = Record<
  SquareNeighborDirection,
  [ColIndex, RowIndex]
>;

export type ColIndex = number;
export type RowIndex = number;
export const SquareNeighborDirectionArr = [
  "T",
  "L",
  "R",
  "B",
  "TL",
  "TR",
  "BL",
  "BR",
] as const;
type SquareNeighborDirection = typeof SquareNeighborDirectionArr[number];

export const ClickableSquare = (props: SquarePropsCallback) => {
  const handleClick = (e: React.MouseEvent<HTMLDivElement>) => {
    e.preventDefault();

    if (e.type === "click") {
      // if left click
      if (e.detail === 1) {
        // if single click
        if (props.clicked || props.marked) {
          return;
        }

        props.onClick([props.colIdx, props.rowIdx]);
      } else if (e.detail === 2) {
        // if double click
        if (!props.clicked) {
          return;
        }

        props.onDoubleClick([props.colIdx, props.rowIdx]);
      }
    } else if (e.type === "contextmenu") {
      // if right click
      if (props.clicked) {
        return;
      }

      props.onRightClick([props.colIdx, props.rowIdx]);
    }
  };

  const handleMouseEnter = (e: React.MouseEvent<HTMLDivElement>) => {
    if (!props.clicked) {
      (e.currentTarget as HTMLDivElement).style.backgroundColor = "dodgerblue";
    }
  };

  const handleMouseLeave = (e: React.MouseEvent<HTMLDivElement>) => {
    if (!props.clicked) {
      (e.currentTarget as HTMLDivElement).style.backgroundColor = "blue";
    }
  };

  return (
    <div
      style={{
        backgroundColor: props.clicked
          ? props.value === -1
            ? props.marked
              ? "lime"
              : "red"
            : "transparent"
          : "blue",
        border: "1px solid black",
        float: "left",
        justifyContent: "center",
        display: "flex",
        alignItems: "center",
      }}
      className="unselectable"
      onClick={handleClick}
      onContextMenu={handleClick}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
    >
      <b>
        {props.clicked
          ? props.value >= 0
            ? props.marked
              ? "❌"
              : props.value > 0
              ? props.value
              : ""
            : "💣"
          : props.marked
          ? "⛳️"
          : ""}
      </b>
    </div>
  );
};
