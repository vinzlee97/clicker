import React, { useEffect, useMemo, useState } from "react";
import { usePrevious } from "../hooks";
import {
  ClickableSquare,
  ColIndex,
  RowIndex,
  SquareProps,
} from "./ClickableSquare";

export interface BoardProps {
  width: number;
  height: number;
  noOfBombs: number;
}

export default function Board({
  width: boardWidth,
  height: boardHeight,
  noOfBombs: boardNoOfBombs,
}: BoardProps) {
  const [squareArr, setSquareArr] = useState<SquareProps[][]>([]);

  // Restart the same previous game (first click doesn't generate bombs).
  // Determines whether the same board array is being played, with the bombs' location remains (when the value is true),
  // or different otherwise.
  const [restarted, setRestarted] = useState<boolean>(false);

  // Victory state.
  // Null - game in progress,
  // True or false - game over (victory or defeat).
  const [isVictory, setVictory] = useState<boolean | null>(false);

  // Remaining mark counts
  const [remainingMarks, setRemainingMarks] = useState<number>(0);

  // Previous restarted value to indicate that it was false on new game
  // before first click.
  const { restarted: prevRestarted } = usePrevious({ restarted }) ?? {};

  const generateBombs = ([clickCol, clickRow]: [ColIndex, RowIndex]) => {
    let bombIdx = 0;

    while (bombIdx < boardNoOfBombs) {
      const rndRow = Math.floor(Math.random() * boardHeight);
      const rndCol = Math.floor(Math.random() * boardWidth);

      const bombInsideFirstClickNeighbors = Object.entries(
        squareArr[clickRow][clickCol].neighbors
      ).some(
        ([_, [neighborCol, neighborRow]]) =>
          neighborRow === rndRow && neighborCol === rndCol
      );

      if (
        (rndRow !== clickRow || rndCol !== clickCol) &&
        !bombInsideFirstClickNeighbors &&
        squareArr[rndRow][rndCol].value !== -1
      ) {
        squareArr[rndRow][rndCol].value = -1;

        Object.entries(squareArr[rndRow][rndCol].neighbors).forEach(
          ([_, [neighborCol, neighborRow]]) => {
            if (squareArr[neighborRow][neighborCol].value !== -1) {
              squareArr[neighborRow][neighborCol].value++;
            }
          }
        );

        bombIdx++;
      }
    }
  };

  const uncoverNonBombSquares = (clicks: [ColIndex, RowIndex][]) => {
    const openQueue = [...clicks];
    let unmarkedLeft = remainingMarks;
    while (openQueue.length !== 0) {
      const [colIndex, rowIndex] = openQueue[0];

      if (squareArr[rowIndex][colIndex].value === 0) {
        Object.entries(squareArr[rowIndex][colIndex].neighbors).forEach(
          ([_, [neighborCol, neighborRow]]) => {
            const isTupleInWaitList = openQueue.some((val) => {
              return (
                JSON.stringify(val) ===
                JSON.stringify([neighborCol, neighborRow])
              );
            });
            if (
              !squareArr[neighborRow][neighborCol].clicked &&
              !isTupleInWaitList
            ) {
              openQueue.push([neighborCol, neighborRow]);
            }
          }
        );
      }

      squareArr[rowIndex][colIndex].clicked = true;

      if (squareArr[rowIndex][colIndex].marked) {
        squareArr[rowIndex][colIndex].marked = false;
        unmarkedLeft++;
      }

      openQueue.shift();
    }

    setRemainingMarks(unmarkedLeft);
  };

  const onSquareClick = (click: [ColIndex, RowIndex]) => {
    if (isVictory !== null) {
      return;
    }

    if (!restarted) {
      generateBombs(click);

      setRestarted(true);
    }

    const [clickCol, clickRow] = click;

    if (squareArr[clickRow][clickCol].value !== -1) {
      uncoverNonBombSquares([click]);
    } else {
      uncoverBombsAndFalseMarkedSquares();
    }

    gameCheck();
  };

  const uncoverBombsAndFalseMarkedSquares = () => {
    const bombsAndFalseMarked = squareArr
      .flat()
      .filter((val) => val.value === -1 || (val.marked && val.value !== -1));

    bombsAndFalseMarked.forEach((val) => {
      const { rowIdx, colIdx } = val;
      squareArr[rowIdx][colIdx].clicked = true;
    });
  };

  const onSquareRightClick = ([colIdx, rowIdx]: [ColIndex, RowIndex]) => {
    if (isVictory !== null || remainingMarks === 0) {
      return;
    }

    squareArr[rowIdx][colIdx].marked = !squareArr[rowIdx][colIdx].marked;

    if (squareArr[rowIdx][colIdx].marked) {
      setRemainingMarks(remainingMarks - 1);
    } else {
      setRemainingMarks(remainingMarks + 1);
    }

    gameCheck();
  };

  const onSquareDoubleClick = ([colIdx, rowIdx]: [ColIndex, RowIndex]) => {
    if (isVictory !== null) {
      return;
    }

    const markedNeighbors = Object.entries(
      squareArr[rowIdx][colIdx].neighbors
    ).filter((val) => {
      const [neighborCol, neighborRow] = val[1];
      return squareArr[neighborRow][neighborCol].marked;
    });

    const unmarkedNeighbors = Object.entries(
      squareArr[rowIdx][colIdx].neighbors
    ).filter((val) => {
      const [neighborCol, neighborRow] = val[1];
      return !squareArr[neighborRow][neighborCol].marked;
    });

    if (markedNeighbors.length === squareArr[rowIdx][colIdx].value) {
      Object.entries(squareArr[rowIdx][colIdx].neighbors).forEach(
        ([_, [neighborCol, neighborRow]]) => {
          squareArr[neighborRow][neighborCol].clicked =
            !squareArr[neighborRow][neighborCol].marked;
        }
      );

      const falseMarkedNeighborExists = markedNeighbors.some((val) => {
        const [neighborCol, neighborRow] = val[1];
        return squareArr[neighborRow][neighborCol].value !== -1;
      });

      if (falseMarkedNeighborExists) {
        uncoverBombsAndFalseMarkedSquares();
      }

      uncoverNonBombSquares(unmarkedNeighbors.map((val) => val[1]));

      gameCheck();
    }
  };

  const initialSquareArr = useMemo(() => {
    if (restarted) {
      return;
    }

    let arr: SquareProps[][] = [];
    const minOfBombs = Math.floor((5 * boardWidth * boardHeight) / 100);
    const maxOfBombs = Math.floor((40 * boardWidth * boardHeight) / 100);

    if (
      boardWidth < 9 ||
      boardHeight < 9 ||
      Number.isNaN(boardWidth) ||
      Number.isNaN(boardHeight)
    ) {
      return arr;
    }

    if (
      Number.isNaN(boardNoOfBombs) ||
      boardNoOfBombs < minOfBombs ||
      boardNoOfBombs > maxOfBombs
    ) {
      return arr;
    }

    arr = new Array(boardHeight).fill([]).map((_, ridx) => {
      return new Array(boardWidth).fill([]).map((_, cidx) => {
        return new SquareProps(boardHeight, boardWidth, ridx, cidx);
      });
    });

    return arr;
  }, [boardHeight, boardWidth, boardNoOfBombs, restarted]);

  useEffect(() => {
    if (!!initialSquareArr) {
      setSquareArr(initialSquareArr);
    }

    // if the previous restarted value is false and it changes to true
    // (exactly the moment after first-click on a new game),
    // prevent the board from restarting the remaining marks count
    // (in case when the player marks on the board before bomb generation)
    if (prevRestarted || !restarted) {
      setRemainingMarks(isNaN(boardNoOfBombs) ? 0 : boardNoOfBombs);
    }

    setVictory(null);
  }, [initialSquareArr, boardNoOfBombs, prevRestarted, restarted]);

  useEffect(() => {
    setRestarted(false);
  }, [boardHeight, boardWidth, boardNoOfBombs]);

  const handleRestart = (_: React.MouseEvent<HTMLButtonElement>) => {
    squareArr.flat().forEach((val) => {
      val.clicked = false;
      val.marked = false;
    });

    setVictory(null);
    setSquareArr([...squareArr]);
    setRemainingMarks(boardNoOfBombs);
  };

  const handleNewGame = (_: React.MouseEvent<HTMLButtonElement>) => {
    setRestarted(false);
  };

  const gameCheck = () => {
    const bombs = squareArr.flat().filter((val) => val.value === -1);

    const isBombarded = bombs.length > 0 && bombs.every((val) => val.clicked);

    if (isBombarded) {
      // set lose
      setVictory(false);
      return;
    }

    const unclicked = squareArr.flat().filter((val) => !val.clicked);
    if (unclicked.length === bombs.length) {
      // set win
      setVictory(true);
      return;
    }

    setSquareArr([...squareArr]);
  };

  return (
    <>
      <div
        style={{
          visibility: squareArr.flat().length > 0 ? "visible" : "collapse",
        }}
        role="presentation"
        aria-label="Game Presentation"
        test-element-id="game-presentation"
      >
        <div role="presentation" aria-label="Game Board">
          <div
            style={{
              display: "inline-grid",
              gridTemplateColumns: `repeat(${boardWidth}, 25px)`,
              gridTemplateRows: `repeat(${boardHeight}, 25px)`,
              border: "1px solid black",
            }}
            role="grid"
          >
            {squareArr.flat().map((prop) => {
              return (
                <ClickableSquare
                  {...prop}
                  onClick={onSquareClick}
                  onRightClick={onSquareRightClick}
                  onDoubleClick={onSquareDoubleClick}
                />
              );
            })}
          </div>
        </div>
        <span>Bomb Count: {remainingMarks}</span>
      </div>
      <div
        style={{
          visibility: isVictory !== null ? "visible" : "collapse",
        }}
      >
        <h1>{isVictory ? "YOU WIN" : "YOU LOSE"}</h1>
        <button onClick={handleRestart}>Restart</button>
        <button onClick={handleNewGame}>New Game</button>
      </div>
    </>
  );
}
