import { useEffect, useRef } from "react";

const usePrevious = <T>(value: T): T => {
  const ref: any = useRef<T>();

  useEffect(() => {
    ref.current = JSON.parse(JSON.stringify(value));
  }, [value]);

  return ref.current;
}

export { usePrevious };