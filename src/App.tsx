import { ChangeEvent, useState } from "react";
import "./App.css";
import Board from "./components/Board";

function App() {
  const [width, setWidth] = useState<number>(0);
  const [height, setHeight] = useState<number>(0);
  const [noOfBombs, setNoOfBombs] = useState<number>(0);

  const onWidthChange = (e: ChangeEvent<HTMLInputElement>) => {
    setWidth(parseInt(e.target.value));
  };

  const onHeightChange = (e: ChangeEvent<HTMLInputElement>) => {
    setHeight(parseInt(e.target.value));
  };

  const onNoOfBombsChange = (e: ChangeEvent<HTMLInputElement>) => {
    setNoOfBombs(parseInt(e.target.value));
  };

  return (
    <div className="App">
      <form>
        <label htmlFor="width">Width: </label>
        <input
          id="width"
          type="number"
          name="width"
          placeholder="50"
          value={width}
          onChange={onWidthChange}
        />
        <br />
        <label htmlFor="height">Height: </label>
        <input
          id="height"
          type="number"
          name="height"
          placeholder="50"
          value={height}
          onChange={onHeightChange}
        />
        <br />
        <label htmlFor="noOfBombs">No Of Bombs: </label>
        <input
          id="noOfBombs"
          type="number"
          name="noOfBombs"
          placeholder="20"
          value={noOfBombs}
          onChange={onNoOfBombsChange}
        />
        <br />
      </form>

      <Board width={width} height={height} noOfBombs={noOfBombs} />
    </div>
  );
}

export default App;
